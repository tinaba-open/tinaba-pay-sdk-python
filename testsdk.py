from uuid import uuid4
from datetime import datetime

from tinabasdk.exceptions import TinabaException
from tinabasdk.actions import InitCheckout, VerifyCheckout, ConfirmP2PtransferByCapture, RefundCheckout, GetCheckoutList
from tinabasdk.apicontext import ApiContext
from tinabasdk.objects import InitCheckoutRequest, VerifyCheckoutRequest, ConfirmP2PtransferByCaptureRequest, RefundCheckoutRequest, GetCheckoutListRequest
from tinabasdk.callbacks import CheckoutStateCallback

context_dict = {'mode': 'sandbox',
                'version': 'v1',
                'url_template': '{hostname}{base_url}{route}',
                'sandbox': {'timeout': 10,
                            'merchant_id': '6116',
                            'secret': 'QkNZXMheavnBQmAZYSKaafqnK1M47PRyZMyf4p61sox6haYdlQGyrPxIZBam2ncCzl66Y4QBlBzuzZa7dI2No'
                                      'pq+cmPMOSQL31LCKjwyzG00B74YqF7WmN+kLke34L/lXUZhVPdtBw72mv3wTaXYBt7bSfk0A3IETlzYMXGv7o'
                                      'pa0tdnxXMXZTMhyDT9WeHUPzZSabe1WXpXWUdYi1WKnJl4icH+vnql7goUpJeVIq6aVDzP+Z1sqpSc3SyWdrO'
                                      '+riyScOlGyXyPaMHWXR7ER4+wu93DIRNvoZVKD17VVxSiOOiTOFObg61wGlgZM9oBF0e1g7N4r0jEBBkKFqIF'
                                      't5yJiRqaVJM1ZP238ybhjLqGDQoKwsGlgmH7JRpZFkX4SUMTkhOnNiT73JOGxb7KTTs1yCdpcatzkwbWPkrdy'
                                      'yc4YaWieQffCGmslVTRSpnYIRyZcLDz3E3FybvmYU3f8Fijh6HW5T8oCzPbjQW8bRgAAoCRSxGJNyfjqDdNRz'
                                      'PxCYDmuHsbzRTG6KK30Sv9I0HZj3s3WUK/rlLcrvRQ+xPnnNflZZM/d08Yxa8USd1zSrdwdqZv5s/KuKqRfLF'
                                      'U4G0LUvywgYy8oJBpVPJFcO6oAi7v9HTIyytpt6me/+Sy0sftcQj5FJ13XoQKZtXnyvAHtJxGTHgSdvXL8hlZ'
                                      'GTE=',
                            'hostname': 'https://valid.tinaba.tv',
                            'base_url': '/WebApi/tinaba/checkout',
                            'port': 443},
                'live': {'timeout': 10,
                         'merchant_id': 'merch',
                         'secret': 'mysecret',
                         'hostname': 'https://tfull.bancaprofilo.it',
                         'base_url': '/WebApi/tinaba/checkout',
                         'port': 443}}

context = ApiContext(context_dict)

print()
print('====================================== InitCheckout ===========================================')
print()

extid = str(uuid4())

mode = 'ECOMMERCE'

obj = InitCheckoutRequest(externalId=extid,
                          amount='1000',
                          currency='EUR',
                          creationDateTime=datetime.now(),
                          paymentMode=mode,
                          notificationCallback='http://www.example.com',
                          notificationHttpMethod='POST',
                          sendReceiverAddress=True)
                          # description='a',
                          # validTo='asd',
                          # productCodeId='aaaaa',
                          # metadata='hellOo',
                          # backCallback='http://www.example.com',
                          # successCallback='http://www.example.com',
                          # failureCallback='http://www.example.com')
act = InitCheckout(context)
act.body_params = obj

try:
    print('Request: {}'.format(act.body_params))
    result = act.run()
    print('{}: {}'.format(result.__class__.__name__, result.__dict__))
except TinabaException as exc:
    print('Exception: {}, message: {}'.format(exc.__class__.__name__, exc))

print()
print('======================================= VerifyCheckout ==========================================')
print()

act = VerifyCheckout(context)
obj = VerifyCheckoutRequest(externalId=extid)
act.body_params = obj
try:
    print('Request: {}'.format(act.body_params))
    result = act.run()
    print('{}: {}'.format(result.__class__.__name__, result.__dict__))
except TinabaException as exc:
    print('Exception: {}, message: {}'.format(exc.__class__.__name__, exc))

if mode == 'PREAUTH':
    print()
    print('======================================== ConfirmP2PtransferByCapture =========================================')
    print()
    act = ConfirmP2PtransferByCapture(context)
    obj = ConfirmP2PtransferByCaptureRequest(externalId=extid, amount='1000')
    act.body_params = obj
    try:
        print('Request: {}'.format(act.body_params))
        result = act.run()
        print('{}: {}'.format(result.__class__.__name__, result.__dict__))
    except TinabaException as exc:
        print('Exception: {}, message: {}'.format(exc.__class__.__name__, exc))

print()
print('======================================== RefundCheckout =========================================')
print()

act = RefundCheckout(context)
obj = RefundCheckoutRequest(externalId=extid, amount='1000')
act.body_params = obj
try:
    print('Request: {}'.format(act.body_params))
    result = act.run()
    print('{}: {}'.format(result.__class__.__name__, result.__dict__))
except TinabaException as exc:
    print('Exception: {}, message: {}'.format(exc.__class__.__name__, exc))

print()
print('========================================= GetCheckoutList ========================================')
print()

act = GetCheckoutList(context)
obj = GetCheckoutListRequest(dateFrom='2017-10-10', dateTo='2018-10-10')
act.body_params = obj
try:
    print('Request: {}'.format(act.body_params))
    result = act.run()
    print('{}: {}'.format(result.__class__.__name__, result.__dict__))
except TinabaException as exc:
    print('Exception: {}, message: {}'.format(exc.__class__.__name__, exc))

print()
print('========================================= Callbacks ========================================')
print()

callback_body = {'externalId': 'myexternalid',
                 'checkoutState': '',
                 'signature': 'DCk2PudlM2Mur72t2RvRH13ZFNVSnD/G8qg6uZSs00A=',
                 'userAddress': {'name': 'Sherlock',
                                 'surname': 'Holmes',
                                 'email': 'sherlock.holmes@gmail.com',
                                 'shippingAddress': {'receiverName': 'John Watson',
                                                     'address': 'Baker Street',
                                                     'streetNumber': '221b',
                                                     'city': 'London',
                                                     'cap': '00100',
                                                     'district': '',
                                                     'country': 'England',
                                                     'sendAt': None,
                                                     'phoneNumber': None},
                                 'billingAddress': {'receiverName': 'John Watson',
                                                    'address': 'Baker Street',
                                                    'streetNumber': '221b',
                                                    'city': 'London',
                                                    'district': '',
                                                    'cap': '00100',
                                                    'country': 'England',
                                                    'fiscalCode': 'HLMSHR21K15H501D'}}}

CheckoutStateCallback.create(callback_body, context_dict['sandbox']['secret'])

